package controllers;

import com.google.inject.Inject;
import models.Task;
import play.api.mvc.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private FormFactory formFactory;

    @Inject
    public HomeController (FormFactory formFactory) { this.formFactory = formFactory; }

    public Result tasks() {
        return ok(tasks.render(Task.find.all()));
    }

    public Result task(Long id) {
        return ok(task.render(Task.find.byId(id)));
    }

    public Result newTask() {
        Form<Task> taskForm = formFactory.form(Task.class);
        return ok(views.html.create.render(taskForm));
    }

    public Result save() {
        Form<Task> taskForm = formFactory.form(Task.class).bindFromRequest();
        if(taskForm.hasErrors()) {
            return badRequest(views.html.create.render(taskForm));
        }
        taskForm.get().save();
        return redirect(routes.HomeController.tasks());
    }

    public Result delete(Long id) {
        Task.find.byId(id).delete();
        return redirect(routes.HomeController.tasks());
    }


}
